import numpy as np
import tensorflow as tf
import keras
import uproot
import sample_preprocess as sample
import utils_training as utils_train
import sklearn.metrics as metrics

from numpy.random import seed
from keras.layers import BatchNormalization, Layer, TimeDistributed, Dropout
from keras.layers import Dense, Input, ReLU, Masking, LSTM, Embedding, Lambda, Bidirectional, Flatten
from keras.models import Model
from keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.layers.experimental import preprocessing
from keras import backend as K
from keras.models import Sequential
from tensorflow.keras.optimizers import Adam, SGD, RMSprop
from keras.callbacks import ReduceLROnPlateau
from tensorflow.keras import regularizers
import matplotlib.pyplot as plt


import tf2onnx
import onnx


######################## Prepare training inputs#################################

sample_in = sample.samples()

comb_list_inputs = sample_in.comb_list_inputs

Y_sig_class = np.array(sample_in.Y_sig_class)
Y_sig_class_cat = np.array(sample_in.Y_sig_class_cat)

eventNumber = np.array(sample_in.eventNumber_in)
event_weight = np.array(sample_in.event_weight)

print ("Shape of inputs = ")
print (comb_list_inputs.shape)

print ("Shape of target = ")
print (Y_sig_class_cat.shape)


######################## Split X and Y into train/test datasets using eventNumber###############


eventTot = len(comb_list_inputs)
p = 0
q = 0
nbr_pairs = 0
for i in range(0,eventTot):
        if (eventNumber[i]%2 == 0):
                nbr_pairs += 1
print (nbr_pairs)


X_train_input = np.copy(comb_list_inputs[0:nbr_pairs])
X_test_input = np.copy(comb_list_inputs[0:eventTot-nbr_pairs])

Y_train = np.copy(Y_sig_class_cat[0:nbr_pairs])
Y_test = np.copy(Y_sig_class_cat[0:eventTot-nbr_pairs])

weight_train= np.copy(event_weight[0:nbr_pairs])
weight_test = np.copy(event_weight[0:eventTot-nbr_pairs])


for i in range(eventTot):
    if (eventNumber[i]%2 == 0):
        X_train_input[p] = np.copy(comb_list_inputs[i])
        Y_train[p] = np.copy(Y_sig_class_cat[i])
        weight_train[p] = np.copy(event_weight[i])
        p += 1

    else:
        X_test_input[q] = np.copy(comb_list_inputs[i])
        Y_test[q] = np.copy(Y_sig_class_cat[i])
        weight_test[q] = np.copy(event_weight[i])
        q += 1

print ("Shape of train dataset = ")
print (X_train_input.shape)

print ("Shape of test dataset = ")
print (X_test_input.shape)

#normalise input features:

#######################################################################################

X_train_input_mean = np.mean(X_train_input)
X_train_input_std = np.std(X_train_input)
np.savez("scaler_train.npz", mean = X_train_input_mean, std = X_train_input_std)

X_test_input_mean = np.mean(X_test_input)
X_test_input_std = np.std(X_test_input)
np.savez("scaler_test.npz", mean = X_test_input_mean, std = X_test_input_std)

scaler_data_train = np.load('scaler_train.npz')
scaler_data_test = np.load('scaler_test.npz')


####################################Training model###########################

nEvnt, nComb, nFeatures = X_train_input.shape

ppm_sizes_int = [640, 640, 640]
dense_sizes_int = [328, 328, 328]

batch_norm = True
dropout = 0.5
nClasses = 10

regularizer = tf.keras.regularizers.L2(1e-4)


scaler_train_final = utils_train.StandardScaler(scaler_data_train['mean'], scaler_data_train['std'])

in_inputs = Input(shape=(nComb, nFeatures))
m_inputs = scaler_train_final.transform(in_inputs) ##saving input normalisation in the model
tdd = m_inputs

for i, phi_nodes in enumerate(ppm_sizes_int):

    tdd = TimeDistributed(Dense(phi_nodes, activation='relu', kernel_regularizer=regularizers.l1_l2(
        l1=1e-5, l2=1e-4), bias_regularizer=regularizers.l2(1e-4), activity_regularizer=regularizers.l2(1e-5)), name="Phi{}_Dense".format(i))(tdd)
    if batch_norm:
        tdd = TimeDistributed(BatchNormalization(),
                              name="Phi{}_BatchNormalization".format(i))(tdd)
    if dropout != 0:
        tdd = TimeDistributed(Dropout(rate=dropout),
                              name="Phi{}_Dropout".format(i))(tdd)
    tdd = TimeDistributed(ReLU(), name="Phi{}_ReLU".format(i))(tdd)

F = utils_train.Sum(name="Sum")(tdd)


for j, (F_nodes, p) in enumerate(zip(dense_sizes_int,
                                     [dropout]*len(dense_sizes_int[:-1])+[0])):

    F = Dense(F_nodes, activation='relu', name="F{}_Dense".format(j))(F)
    if batch_norm:
        F = BatchNormalization(name="F{}_BatchNormalization".format(j))(F)
    if dropout != 0:
        F = Dropout(rate=p, name="F{}_Dropout".format(j))(F)
    F = ReLU(name="F{}_ReLU".format(j))(F)


    
output = Dense(nClasses, activation='softmax',name="Output")(F)
score_list = tf.unstack(output, axis=-1)
y_class = tf.argmax(output, axis=-1, output_type=tf.int32)
output_list = score_list
ds_MVA = tf.keras.Model(inputs=in_inputs,outputs=output_list)


ds_MVA.summary()



###################################Load model############################################

ds_MVA.load_weights("training_with_evenEvenNumber.h5")

ds_MVA.compile(
    loss=tf.keras.losses.CategoricalCrossentropy(),
    optimizer=tf.keras.optimizers.Adam(learning_rate=1e-4),
    metrics=tf.keras.metrics.CategoricalAccuracy(name='accuracy')
)
    

############################Convert to ONNX ###################################################

class_labels = ["ttH_0_60", "ttH_60_120", "ttH_120_200", "ttH_200_300", "ttH_300_450", "ttH_450_inf", "tt_1b", "tt_B", "tt_2b", "tt_c"]


onnx_model = tf2onnx.convert.from_keras(ds_MVA)[0]

# Rename outputs
output_names = [out.name for out in onnx_model.graph.output]
new_out_names =[f'{label}' for label in class_labels]

if len(output_names) != len(new_out_names):
    raise ValueError("Class labels metadata doesn't correspond with the model outputs")
for old_name, new_name in zip(output_names, new_out_names):
    utils_train.rename_tensor(onnx_model, old_name, new_name) ###### renaming output class corresponding to names

onnx.save(onnx_model,'model_with_evenEvenNumber.onnx')



###################sample of opening  ONNX model###########################3


#import onnxruntime as ort
#sess = ort.InferenceSession("model_with_evenEvenNumber.onnx")

#input_name = sess.get_inputs()[0].name
#output_name = sess.get_outputs()[0].name

#print(input_name)
#print(output_name)

#pred_onxx = sess.run([output_name],{input_name: X_train_input.astype(np.float32)})[0]


