import sklearn
import keras
import tensorflow as tf
from keras.layers import BatchNormalization, Layer, TimeDistributed, Dropout, Input
from keras.layers import Dense, Input, ReLU, Masking, LSTM, Embedding, Lambda, Bidirectional, Flatten
from keras.models import Model

from keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.layers.experimental import preprocessing
from keras import backend as K
from keras.models import Sequential
from tensorflow.keras.optimizers import Adam, SGD, RMSprop

import matplotlib as mpl
from numpy.polynomial.polynomial import polyfit
from keras.callbacks import ReduceLROnPlateau
from numpy.random import seed
import matplotlib.pyplot as plt

from sklearn.metrics import roc_curve, auc, roc_auc_score
from sklearn.preprocessing import LabelBinarizer
import sklearn.metrics as metrics
from sklearn.metrics import classification_report, confusion_matrix

from sklearn.metrics import confusion_matrix
from tensorflow.keras import regularizers

##sum layer for Deep-sets

class Sum(Layer):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.supports_masking = True

    def build(self, input_shape):
        pass

    def call(self, x, mask=None):
        if mask is not None:
            x = x * K.cast(mask, K.dtype(x))[:, :, None]
        return K.sum(x, axis=1)

    def compute_output_shape(self, input_shape):
        return input_shape[0], input_shape[2]

    def compute_mask(self, inputs, mask):
        return None


##performance metric

class CategoricalTruePositives(keras.metrics.Metric):
    def __init__(self, name="categorical_true_positives", **kwargs):
        super(CategoricalTruePositives, self).__init__(name=name, **kwargs)
        self.true_positives = self.add_weight(name="ctp", initializer="zeros")

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_pred = tf.reshape(tf.argmax(y_pred, axis=1), shape=(-1, 1))
        values = tf.cast(y_true, "int32") == tf.cast(y_pred, "int32")
        values = tf.cast(values, "float32")
        if sample_weight is not None:
            sample_weight = tf.cast(sample_weight, "float32")
            values = tf.multiply(values, sample_weight)
        self.true_positives.assign_add(tf.reduce_sum(values))

    def result(self):
        return self.true_positives

    def reset_state(self):
        # The state of the metric will be reset at the start of each epoch.
        self.true_positives.assign(0.0)



def balance_class(sample_weight, Y):
  tot, cat = Y.shape
  w1 = (sample_weight[Y[:, 0]==1].sum() + sample_weight[Y[:, 1]==1].sum() + sample_weight[Y[:, 2]==1].sum() + sample_weight[Y[:, 3]==1].sum() + sample_weight[Y[:, 4]==1].sum() + sample_weight[Y[:, 5]==1].sum())
  w2 = (sample_weight[Y[:, 6]==1].sum() + sample_weight[Y[:, 7]==1].sum() + sample_weight[Y[:, 8]==1].sum() + sample_weight[Y[:, 9]==1].sum())
  w = w2/w1
  sample_weight[Y[:, 0]==1] = sample_weight[Y[:, 0]==1]*w
  sample_weight[Y[:, 1]==1] = sample_weight[Y[:, 1]==1]*w
  sample_weight[Y[:, 2]==1] = sample_weight[Y[:, 2]==1]*w
  sample_weight[Y[:, 3]==1] = sample_weight[Y[:, 3]==1]*w
  sample_weight[Y[:, 4]==1] = sample_weight[Y[:, 4]==1]*w
  sample_weight[Y[:, 5]==1] = sample_weight[Y[:, 5]==1]*w

  return sample_weight

     


class StandardScaler:
    def __init__(self, mean, stdev):
        self.mean = tf.constant(mean, dtype=tf.float32)
        self.stdev = tf.constant(stdev, dtype=tf.float32)

    def transform(self, x):
        y = x - self.mean
        y = y / self.stdev
        return y


def rename_tensor(model, old_name, new_name):
    # Check name duplicates
    for node in model.graph.node:
        if new_name in node.input or new_name in node.output:
            raise ValueError(f"Name '{new_name}' already exists in graph.")

    for graph_input in model.graph.input:
        if graph_input.name == new_name:
            raise ValueError(f"Name '{new_name}' already exists in graph.")

    for graph_output in model.graph.output:
        if graph_output.name == new_name:
            raise ValueError(f"Name '{new_name}' already exists in graph.")

    for initializer in model.graph.initializer:
        if initializer.name == new_name:
            raise ValueError(f"Name '{new_name}' already exists in graph.")

    # Rename
    for node in model.graph.node:
        for i in range(len(node.input)):
            if node.input[i] == old_name:
                node.input[i] = new_name

        for i in range(len(node.output)):
            if node.output[i] == old_name:
                node.output[i] = new_name

    for graph_input in model.graph.input:
        if graph_input.name == old_name:
            graph_input.name = new_name

    for graph_output in model.graph.output:
        if graph_output.name == old_name:
            graph_output.name = new_name

    for initializer in model.graph.initializer:
        if initializer.name == old_name:
            initializer.name = new_name
                
