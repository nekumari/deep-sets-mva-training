# Deep-sets MVA for ttHbb analysis

#Building the inputs for the training

"jetCombination_featureMaker.C" builds the jet combination features used in training for different ttH and tt+jets samples

**setupATLAS

lsetup root

root -l 

.L  jetCombination_featureMaker.C++

jet_comb("ttH_mc16a.root","output.root")**



########merge all the output root files

hadd -f output_merge.root    ttH_mc16a.root ttH_mc16d.root ....ttbb_mc16a.root...

########## TRAINING#########

**source create_en.sh

python run_train.py**

#### "weight.h5" file is generated 


Saving the model  and CONVERT TO ONNX using trained weights

**python convert_to_ONNX.py **

###This example is for training over events with even eventNumber. Similar training steps needs to be done for training over events with odd eventNumber.
