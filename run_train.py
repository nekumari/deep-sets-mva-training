import numpy as np
import tensorflow as tf
import keras
import uproot
import sample_preprocess as sample
import utils_training as utils_train
import sklearn.metrics as metrics

from numpy.random import seed
from keras.layers import BatchNormalization, Layer, TimeDistributed, Dropout
from keras.layers import Dense, Input, ReLU, Masking, LSTM, Embedding, Lambda, Bidirectional, Flatten
from keras.models import Model
from keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.layers.experimental import preprocessing
from keras import backend as K
from keras.models import Sequential
from tensorflow.keras.optimizers import Adam, SGD, RMSprop
from keras.callbacks import ReduceLROnPlateau
from tensorflow.keras import regularizers
import matplotlib.pyplot as plt



######################## Prepare training inputs#################################

sample_in = sample.samples()

comb_list_inputs = sample_in.comb_list_inputs

Y_sig_class = np.array(sample_in.Y_sig_class)
Y_sig_class_cat = np.array(sample_in.Y_sig_class_cat)

eventNumber = np.array(sample_in.eventNumber_in)
event_weight = np.array(sample_in.event_weight)

print ("Shape of inputs = ")
print (comb_list_inputs.shape)

print ("Shape of target = ")
print (Y_sig_class_cat.shape)

#shuffle dataset

randomize = np.arange(len(comb_list_inputs))
np.random.shuffle(randomize)

comb_list_inputs = comb_list_inputs[randomize]
Y_sig_class = Y_sig_class[randomize]
Y_sig_class_cat = Y_sig_class_cat[randomize]
eventNumber = eventNumber[randomize]
event_weight = event_weight[randomize]


######################## Split X and Y into train/test datasets using eventNumber###############


eventTot = len(comb_list_inputs)
p = 0
q = 0
nbr_pairs = 0
for i in range(0,eventTot):
        if (eventNumber[i]%2 == 0):
                nbr_pairs += 1
print (nbr_pairs)


X_train_input = np.copy(comb_list_inputs[0:nbr_pairs])
X_test_input = np.copy(comb_list_inputs[0:eventTot-nbr_pairs])

Y_train = np.copy(Y_sig_class_cat[0:nbr_pairs])
Y_test = np.copy(Y_sig_class_cat[0:eventTot-nbr_pairs])

weight_train= np.copy(event_weight[0:nbr_pairs])
weight_test = np.copy(event_weight[0:eventTot-nbr_pairs])


for i in range(eventTot):
    if (eventNumber[i]%2 == 0):
        X_train_input[p] = np.copy(comb_list_inputs[i])
        Y_train[p] = np.copy(Y_sig_class_cat[i])
        weight_train[p] = np.copy(event_weight[i])
        p += 1

    else:
        X_test_input[q] = np.copy(comb_list_inputs[i])
        Y_test[q] = np.copy(Y_sig_class_cat[i])
        weight_test[q] = np.copy(event_weight[i])
        q += 1

print ("Shape of train dataset = ")
print (X_train_input.shape)

print ("Shape of test dataset = ")
print (X_test_input.shape)

#normalise input features:

X_train_input -= np.mean(X_train_input) # center
X_train_input/= np.std(X_train_input) # normalize

X_test_input -= np.mean(X_test_input) # center
X_test_input/= np.std(X_test_input) # normalize

################################rescale ttH to tt+jets weighted events######

rescaled_weight_train = np.absolute(utils_train.balance_class(weight_train,Y_train))
rescaled_weight_test = np.absolute(utils_train.balance_class(weight_test,Y_test))


####################################Training model###########################

nEvnt, nComb, nFeatures = X_train_input.shape

ppm_sizes_int = [640, 640, 640]
dense_sizes_int = [328, 328, 328]

batch_norm = True
dropout = 0.5
nClasses = 10

regularizer = tf.keras.regularizers.L2(1e-4)


in_inputs = Input(shape=(nComb, nFeatures))
tdd = in_inputs

for i, phi_nodes in enumerate(ppm_sizes_int):

    tdd = TimeDistributed(Dense(phi_nodes, activation='relu', kernel_regularizer=regularizers.l1_l2(
        l1=1e-5, l2=1e-4), bias_regularizer=regularizers.l2(1e-4), activity_regularizer=regularizers.l2(1e-5)), name="Phi{}_Dense".format(i))(tdd)
    if batch_norm:
        tdd = TimeDistributed(BatchNormalization(),
                              name="Phi{}_BatchNormalization".format(i))(tdd)
    if dropout != 0:
        tdd = TimeDistributed(Dropout(rate=dropout),
                              name="Phi{}_Dropout".format(i))(tdd)
    tdd = TimeDistributed(ReLU(), name="Phi{}_ReLU".format(i))(tdd)

F = utils_train.Sum(name="Sum")(tdd)


for j, (F_nodes, p) in enumerate(zip(dense_sizes_int,
                                     [dropout]*len(dense_sizes_int[:-1])+[0])):

    F = Dense(F_nodes, activation='relu', name="F{}_Dense".format(j))(F)
    if batch_norm:
        F = BatchNormalization(name="F{}_BatchNormalization".format(j))(F)
    if dropout != 0:
        F = Dropout(rate=p, name="F{}_Dropout".format(j))(F)
    F = ReLU(name="F{}_ReLU".format(j))(F)

output = Dense(nClasses, activation='softmax', name="Output")(F)
ds_MVA = Model(inputs=in_inputs, outputs=output)

ds_MVA.summary()

#ds_MVA.load_weights("training_with_evenEvenNumber.h5") ### uncomment when loading already trained weights 

######################Compile model#############################################

ds_MVA.compile(
    loss=tf.keras.losses.CategoricalCrossentropy(),
    optimizer=tf.keras.optimizers.Adam(learning_rate=1e-4),
    metrics=tf.keras.metrics.CategoricalAccuracy(name='accuracy'),
    weighted_metrics=['accuracy', keras.metrics.AUC(name="auc")],
    #metrics=[keras.metrics.CategoricalAccuracy(),utils_train.CategoricalTruePositives()] #additional checks
)


dips_mChkPt = ModelCheckpoint('training_with_evenEvenNumber.h5', monitor='val_loss', verbose=True,
                              save_best_only=True,
                              save_weights_only=True)

callbacks = [

    tf.keras.callbacks.EarlyStopping(monitor='val_loss', mode='min',
                                     min_delta=0, patience=10,
                                     restore_best_weights=True)
]


######################Fit  model################################################

history = ds_MVA.fit(X_train_input, Y_train, epochs=1,validation_split=0.2, shuffle=True, batch_size=64, sample_weight=rescaled_weight_train,callbacks=[callbacks, dips_mChkPt],verbose=True) ##epoch set to 1 for testing

epochs = np.arange(1,len(history.history['loss'])+1)
plt.plot(epochs,history.history['loss'],label='training')
plt.plot(epochs,history.history['val_loss'],label='validation')
plt.xlim([0, len(history.history['loss'])+1])
plt.xlabel('epochs',fontsize=14)
plt.ylabel('loss',fontsize=14)
plt.legend()
plt.title('>=6 nJets >=4 b-jet@70%')
plt.savefig('epoch_train.png')
#plt.show()


######################Saving prediction to root file for checks################

#ypred = ds_MVA.predict(X_test_input, batch_size = 64)
#ypred_max = (np.argmax(ds_MVA.predict(X_test_input),axis=1))
#file = uproot.recreate("pred_trained_onTrain.root")
#file["nominal_Loose"] = {"ypred": np.array(ypred),"ypred_max": np.array(ypred_max)}




