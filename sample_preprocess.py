import numpy as np
import uproot3
import uproot
import tensorflow as tf
from keras.utils import np_utils

class samples():

   #retreiving variables from root file via uproot
    
  tree = uproot3.open("/atlas/kumari/TTHBB/macros/out_jetcomb_latest_test.root")["nominal_Loose"]


  comb_list = []
  comb_var = ["comb_higgs_pt","comb_higgs_mass","comb_hadW_mass","comb_hadtop_mass","comb_leptop_mass","comb_hadWblepTop_mass","comb_minbhadTopqhadW_dR","comb_hadWblepTop_dR","comb_blepTopbhadTop_dR","comb_bhadTopq2hadW_dR","comb_hadWbhadTop_dR","comb_Higgsq1hadW_mass","comb_bbHiggs_dR","comb_bhadTopq1hadW_dR","comb_qqhadW_dR","comb_diff_mindRbhadTopqhadW_dRlepblepTop","comb_lepbhadTop_dR","comb_lepb1Higgs_dR","comb_lepWbhadTop_mass","comb_lepblepTop_dR","Mbb_MindR_Sort4","dRbb_MaxPt_Sort4","H1_all","dRbb_avg_Sort4","nHiggsbb30_Sort4","dEtajj_MaxdEta","Aplanarity_jets","comb_bbHiggs_tagWeightBin_sum","comb_jet_tagWeightBin_order_3_tagWeightBin","comb_jet_tagWeightBin_order_4_tagWeightBin","comb_jet_tagWeightBin_order_5_tagWeightBin","comb_higgs_ttbar_dR","comb_higgs_leptop_dR","comb_higgs_bleptop_mass"]
  
  #inputs for training
  for l in range(0,len(comb_var)):
    comb_list.append(tree[comb_var[l]].array())

  # retreiving other variables
  
  m_event_weights = tree["weight_mc_new"].array()
  m_eventNumber = tree["eventNumber"].array()
  m_nBTags_DL1r70 = tree["nBTags_DL1r70"].array()
  m_nJets = tree["nJets"].array()
  m_HF_Classification = tree["HF_Classification"].array()
  m_HF_SimpleClassification = tree["HF_SimpleClassification"].array()
  m_TTHClassBDTOutput_2017paper = tree["TTHClassBDTOutput_2017paper"].array()
  m_higgs_truthPt = tree["higgs_truthPt"].array()
  m_best_higgs_Pt_withH = tree["best_higgs_Pt_withH"].array()
  m_weight_bTagSF_DL1r_Continuous = tree["weight_bTagSF_DL1r_Continuous"].array()
  m_weight_normalise = tree["weight_normalise"].array()
  m_weight_pileup = tree["weight_pileup"].array()
  m_weight_jvt = tree["weight_jvt"].array()
  m_weight_leptonSF = tree["weight_leptonSF"].array()
  m_randomRunNumber = tree["randomRunNumber"].array()

############################selections######################################## 

  comb_list_in = []
  target_class = []

  higgs_truthPt_in = []
  eventNumber_in = []
  nBTags_DL1r70_in = []
  nJets_in = []
  HF_Classification_in = []
  HF_SimpleClassification_in = []
  TTHClassBDTOutput_2017paper_in = []
  best_higgs_Pt_withH_in = []
  event_weights_in = []
  weight_bTagSF_DL1r_Continuous_in = []
  weight_normalise_in = []
  weight_pileup_in = []
  weight_jvt_in = []
  weight_leptonSF_in = []
  randomRunNumber_in = []

  pass_no = 0
  count = []


 #filling events with >=6jets and >=4@70% and events with 12/24 combinations 

  for i in range(0,len(m_higgs_truthPt)):
    if (m_nJets[i]>=6 and m_nBTags_DL1r70[i]>=4):   
      if (len(comb_list[0][i])>=0):
      
        pass_no = pass_no + 1
        count.append(pass_no)

      
        # fill inputs here after selection

        higgs_truthPt_in.append(m_higgs_truthPt[i])
        event_weights_in.append(m_event_weights[i])
        best_higgs_Pt_withH_in.append(m_best_higgs_Pt_withH[i])
        eventNumber_in.append(m_eventNumber[i])
        nBTags_DL1r70_in.append(m_nBTags_DL1r70[i])
        nJets_in.append(m_nJets[i])
        HF_Classification_in.append(m_HF_Classification[i])
        HF_SimpleClassification_in.append(m_HF_SimpleClassification[i])
        TTHClassBDTOutput_2017paper_in.append(m_TTHClassBDTOutput_2017paper[i])
        weight_bTagSF_DL1r_Continuous_in.append(m_weight_bTagSF_DL1r_Continuous[i])
        weight_normalise_in.append(m_weight_normalise[i])
        weight_pileup_in.append(m_weight_pileup[i])
        weight_jvt_in.append(m_weight_jvt[i])
        weight_leptonSF_in.append(m_weight_leptonSF[i])
        randomRunNumber_in.append(m_randomRunNumber[i])
        
        
        for j in range(0,len(comb_var)):
         if (len(comb_list[j][i])!= 24):
           add = np.pad(comb_list[j][i][:], (0, 12), 'constant') #padding with zeros for events with combination==12
           comb_list_in.append(add)
         else:
           comb_list_in.append(comb_list[j][i][:])  

    

  #reshaping inputs
  comb_list_inputs = np.copy((comb_list_in))
  comb_list_inputs = comb_list_inputs.reshape(len(count),len(comb_var),24)
  comb_list_inputs = np.transpose(comb_list_inputs, (0, 2, 1))
  print (comb_list_inputs.shape)

  ########################building truth classes################################
  
  Y_all = np.copy(higgs_truthPt_in)#truth higgs pt
  print (Y_all.shape)


  for k in range(0, len(Y_all)):
    if Y_all[k] >= 0 and Y_all[k] <= 60000:
        target_class.append(0)

    if Y_all[k] > 60000 and Y_all[k] <= 120000:
        target_class.append(1)
       
    if Y_all[k] > 120000 and Y_all[k] <= 200000:
        target_class.append(2)
      
    if Y_all[k] > 200000 and Y_all[k] <= 300000:
        target_class.append(3)
        
    if Y_all[k] > 300000 and Y_all[k] <= 450000:
        target_class.append(4)
       
    if Y_all[k] > 450000:
        target_class.append(5)

    if Y_all[k]==-8:
       
      if (np.copy(HF_SimpleClassification_in)[k]==1):
        if ((np.array(HF_Classification_in)[k] >= 1000) and (np.array(HF_Classification_in)[k] < 1100)):
           target_class.append(6)         

        if ((np.array(HF_Classification_in)[k] >= 100) and (np.array(HF_Classification_in)[k] < 200)):
           target_class.append(7)
           

        if (((np.array(HF_Classification_in)[k] >= 200) and (np.array(HF_Classification_in)[k] < 1000)) or (np.array(HF_Classification_in)[k] >= 1100)):
           target_class.append(8)
           
      if (np.array(HF_SimpleClassification_in)[k]!=1):
        if (np.array(HF_SimpleClassification_in)[k]==-1):
           target_class.append(9)
  

  Y_sig_class = np.copy(target_class)
  Y_sig_class_cat = np_utils.to_categorical(Y_sig_class, 10)
  print(Y_sig_class_cat.shape)

    
  ##################event weights for training#################################

  event_weight = np.array(weight_normalise_in) * np.array(weight_pileup_in) * np.array(event_weights_in) * np.array(weight_leptonSF_in) * np.array(weight_bTagSF_DL1r_Continuous_in) * np.array(weight_jvt_in) * np.where(np.array(randomRunNumber_in)<= 311481, 36207.66,np.where(np.array(randomRunNumber_in) <= 340453, 44307.4, 58450.1))  

  #############################################################################
  
  def variable_save(self):
    return self.comb_var
    return self.eventNumber_in
    return self.comb_list_inputs
    return self.Y_sig_class
    return self.Y_sig_class_cat
    return self.event_weight
